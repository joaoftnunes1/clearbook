import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import Signup from '../containers/singup';

test('render fields step 1', async () => {
  const { getByText, } = render(<Signup />);
  await waitFor(() => {
    expect(getByText('Abra sua conta na Clearbook')).toBeInTheDocument();
    expect(getByText('Em poucos minutos você abre a sua conta e pode investir em projetos inovadores')).toBeInTheDocument();
    expect(getByText('Passo 1 de 3')).toBeInTheDocument();
    expect(getByText('Dados pessoais')).toBeInTheDocument();
    expect(getByText('Todos os campos são obrigatórios')).toBeInTheDocument();
    expect(getByText('E-mail')).toBeInTheDocument();
    expect(getByText('Senha')).toBeInTheDocument();
  });
});

test('render step 1 valid data', async () => {
  const data:any = {
    email: 'joaoftnunes@gmail.com',
    passowrd: 'A1',
    cpf: '06278059576',
    birthdate: '11/07/1995'
  }
  const { getByTestId } = render(<Signup formData={data} />);
  await waitFor(() => {
    expect(getByTestId('btnSubmit')).toBeDisabled();
  });
});

test('render step 1 invalid data', async () => {
  const data:any = {
    email: 'joaoftnunes@gmail.com',
    passowrd: 'aa',
    cpf: '06278059576',
    birthdate: '11/07/1995'
  }
  const { getByText, } = render(<Signup formData={data} />);
  await waitFor(() => {
    expect(getByText(/CONTINUAR/i).closest('button')).toBeDisabled();
  });
});
