import * as Yup from "yup";
export const schemaStep1 = Yup.object().shape({
    email: Yup.string()
        .email()
        .min(1)
        .required(),
    password: Yup.string()
    .min(1)
    .test('password', '', val => val?.match(/.*[A-Z]/) != null && val?.match(/.*[1-9]/) != null)
    .required()
})

export const schemaStep2 = Yup.object().shape({
    cpf: Yup.string().test('cpf', '', val => val?.match(/\d/g)?.join('').length === 11)
        .required(),
    birthdate: Yup.string().min(10).test('birthdate', '', val => val?.match(/\d/g)?.join('').length === 8).required()
})
