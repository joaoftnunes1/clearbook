import logo from '../../../assets/icons/logo.svg'
import styles from '../singup.module.css';

function Header() {
    return (
        <>
            <img alt="logo" src={logo} className={styles.logo}></img>
            <h3>Abra sua conta na Clearbook</h3>
            <p>Em poucos minutos você abre a sua conta e pode investir em projetos inovadores</p>
        </>
    );
}

export default Header;
