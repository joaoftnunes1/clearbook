import successIcon from '../../../assets/icons/success-register.svg'
import styles from '../singup.module.css';
function Success() {
    return (
        <>
            <div className={styles.containerFormTitle}>
                <h4>Cadastro feito com sucesso!</h4>
                <p>Tudo certo por aqui, você já pode começar a investir.</p>
            </div>
            <img alt='success' src={successIcon} className={styles.imgSuccess}></img>
        </>
    );
}

export default Success;
