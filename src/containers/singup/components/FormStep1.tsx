import { Input } from 'antd';
import styles from '../singup.module.css';
import { getValue, onChange } from '../../../util/inputUtil'
function FormStep1({ context }: any) {
    return (
        <>
            <div className={styles.containerFormTitle}>
                <h4>Dados pessoais</h4>
                <p>Todos os campos são obrigatórios</p>
            </div>
            <div className={styles.containerFormData}>
                <label className="formLabel">E-mail</label>
                <Input value={getValue(context, 'email')} onChange={(event) => onChange(context, 'email', event)}></Input>
                <label className="subtitle">
                    Usaremos este e-mail para falar com você
        </label>
                <br />
                <label className="formLabel">Senha</label>
                <Input.Password value={getValue(context, 'password')} onChange={(event) => onChange(context, 'password', event)}></Input.Password>
            </div>
            
        </>
    );
}

export default FormStep1;
