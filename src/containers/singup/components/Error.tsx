import errorIcon from '../../../assets/icons/traffic-cone.svg'
import styles from '../singup.module.css';
function Success() {
    return (
        <>
            <div className={styles.containerFormTitle}>
                <h4>Ops, ocorreu um erro!</h4>
                <p>Por favor, tenta novamente.</p>
            </div>
            <img alt='error' src={errorIcon} className={styles.imgError}></img>
        </>
    );
}

export default Success;
