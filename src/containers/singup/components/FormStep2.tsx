import styles from '../singup.module.css';
import { getValue, onChange } from '../../../util/inputUtil'
import MaskedInput from 'antd-mask-input'
function FormStep2({ context }: any) {
    return (
        <>
            <div className={styles.containerFormTitle}>
                <h4>Informações de acesso</h4>
                <p>Todos os campos são obrigatórios</p>
            </div>
            <div className={styles.containerFormData}>
                <label className="formLabel">Cpf</label>
                <MaskedInput mask="111.111.111-11" value={getValue(context, 'cpf')} onChange={(event) => onChange(context, 'cpf', event)}></MaskedInput>
                <label className="formLabel">Data nascimento</label>
                <MaskedInput mask="11/11/1111" value={getValue(context, 'birthdate')} onChange={(event) => onChange(context, 'birthdate', event)}></MaskedInput>
            </div>
        </>
    );
}

export default FormStep2;
