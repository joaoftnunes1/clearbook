import { useEffect, useState } from 'react';
import Steps from '../../components/Steps'
import styles from './singup.module.css'
import FormStep1 from './components/FormStep1'
import FormStep2 from './components/FormStep2'
import Success from './components/Success'
import Error from './components/Error'
import rightArrow from '../../assets/icons/right-arrow.svg'
import leftArrow from '../../assets/icons/left-arrow.svg'
import Header from './components/Header'
import { Row, Col, Button } from 'antd';
import { useFormik } from 'formik';
import * as validations from './validation'
import _ from 'lodash'
import { request } from '../../util/request'
import * as modules from '../../services/modules/index'
function Singup({
  formData = {
    email: '',
    password: '',
    cpf: '',
    birthdate: ''
  }
}
) {
  const [steps] = useState([1, 2, 3])
  const [currentStep, setCurrentStep] = useState(0)
  const [errorRequest, setErrorRequest] = useState(false)

  function getTitleStep(step: number, steps: Array<number>) {
    return `Passo ${step} de ${steps.length}`
  }
  const formik = useFormik({
    initialValues: formData,
    validationSchema: getSchemaValidation(),
    onSubmit: values => {
      if (currentStep === 1) {
        requestApi()
      } else {
        setCurrentStep(currentStep + 1)
      }
    },
  });
  const { errors } = formik
  function isDisabledBtn() {
    return !_.isEmpty(errors)
  }
  function goBack() {
    setCurrentStep(currentStep - 1)
  }
  function getSchemaValidation() {
    if (currentStep === 0) {
      return validations.schemaStep1
    }
    return validations.schemaStep2
  }
  useEffect(() => {
    formik.validateForm()
  }, [currentStep])

  async function requestApi() {
    try {
      await request(modules.signup((formik.values)))
      setCurrentStep(2)
      setErrorRequest(false)
    } catch (error) {
      console.log(error)
      setErrorRequest(true)
    }
  }
  return (
    <Row justify='center' align='middle'>
      <Col xs={20} md={10} sm={20} lg={8} xxl={8} className={styles.containerForm}>
        <Header></Header>
        <Steps steps={steps} currentStep={currentStep} titleStep={getTitleStep}></Steps>
        {!errorRequest && <>
          {currentStep === 0 && <FormStep1 context={formik}></FormStep1>}
          {currentStep === 1 && <FormStep2 context={formik}></FormStep2>}
          {currentStep === 2 && <Success></Success>}
          {currentStep !== 2 && <div className={styles.containerFooter}>
            {currentStep !== 0 && <Button className={`${styles.btn} ${styles.btnBack}`} onClick={() => goBack()}>
              <img alt="leftarrow" src={leftArrow} className={styles.iconArrow}></img>
              <label className={`${styles.btnTitle} ${styles.btnTitleBack}`}>VOLTAR</label>
            </Button>}
            <Button data-testid="btnSubmit" disabled={isDisabledBtn()} className={`${styles.btn} ${styles.bgPrimary}`} onClick={() => {
              formik.submitForm()
            }}>
              <label className={styles.btnTitle}>CONTINUAR</label>
              <img alt="rightarrow" src={rightArrow} className={styles.iconArrow}></img>
            </Button>
          </div>}
        </>}
        {errorRequest && <>
          <Error></Error>
          <Button className={`${styles.btn} ${styles.bgPrimary}`} onClick={() => requestApi()}>
            <label className={styles.btnTitle}>TENTAR NOVAMENTE</label>
          </Button>
        </>}


      </Col>
    </Row>
  );
}

export default Singup;
