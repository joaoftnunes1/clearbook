import { Steps, Popover } from 'antd';
import styles from './steps.module.css'

const { Step } = Steps;
const customDot = (dot: any, { status, index }: any) => (
  <Popover>
    {dot}
  </Popover>
);
function App(props: any) {
  const { steps, currentStep, titleStep } = props

  return (
    <Steps current={currentStep} progressDot={customDot} className={styles.steps} >
      {steps.map((step: number, index: number) => <Step key={index} title={index === currentStep && titleStep(step, steps)} />)}
    </Steps>
  );
}

export default App;
