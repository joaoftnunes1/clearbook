import { instance } from '../api';
import { AxiosResponse } from 'axios'
export async function signup(requestData: object): Promise<AxiosResponse> {
    const { data } = await instance.post('/endpoint', {
        ...requestData
    });
    return data
};