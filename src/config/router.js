import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Singup from '../containers/singup/index'

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/">
          <Singup></Singup>
        </Route>
      </Switch>
    </Router>
  );
}