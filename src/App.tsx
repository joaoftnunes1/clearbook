import React from 'react';
import Router from './config/router'
import './index.css'
import 'antd/dist/antd.css';
function App() {
  return (
    <Router />
  );
}

export default App;
