import { AxiosResponse } from 'axios'

export async function request(api: Promise<AxiosResponse>) {
    try {
        const response = await api
        return response
    } catch (error) {
        console.log(error)
        throw error;
    }
}