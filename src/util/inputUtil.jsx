export function getValue(formik, fieldName) {
    return formik.values[fieldName]
}

export function onChange(formik, fieldName, event) {
    formik.setFieldValue(fieldName, event.target.value)
}